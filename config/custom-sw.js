/* eslint-disable no-undef */
/* eslint-disable no-restricted-globals */
self.addEventListener('install', event => event.waitUntil(self.skipWaiting()));

self.addEventListener('activate', event => event.waitUntil(self.clients.claim()));

self.addEventListener('message', event => event.waitUntil(self.skipWaiting()));

workbox.precaching.precacheAndRoute(self.__precacheManifest);
console.log(self.__precacheManifest)

workbox.routing.registerRoute(
  new RegExp("/api/"),
  new workbox.strategies.NetworkFirst());
