'use strict';

const express = require('express');
const redirectToHTTPS = require('express-http-to-https').redirectToHTTPS;

// CODELAB: Change this to add a delay (ms) before the server responds.
function apiPwa(req, resp) {
  resp.json({'remote_id': `${Math.random().toPrecision(6).substring(2)}`});
}

/**
 * Starts the Express server.
 *
 * @return {ExpressServer} instance of the Express server.
 */
function startServer() {
  const app = express();

  // Redirect HTTP to HTTPS,
  app.use(redirectToHTTPS([/localhost:(\d{4})/, /0.0.0.0:(\d{4})/], [], 301));

  // Logging for each request
  app.use((req, resp, next) => {
    const now = new Date();
    const time = `${now.toLocaleDateString()} - ${now.toLocaleTimeString()}`;
    const path = `"${req.method} ${req.path}"`;
    const m = `${req.ip} - ${time} - ${path}`;
    next();
  });

  // Handle requests for the data
  app.get('/api/pwa', apiPwa);

  // Handle requests for static files
  app.use(express.static('build'));

  // Start the server
  return app.listen('8000', '0.0.0.0');
}

startServer();
