import React from 'react';
import axios from 'axios';
import logo from './logo.svg';
import './App.css';


class App extends React.Component<{}, {}> {
  public state: {
    name: string
  } = {
    name: ''
  }
  constructor (props: any) {
    super(props)
  }
  public componentDidMount () {
    this.click()
  }

  public click = () => {
    axios.get('/api/pwa').then((res:any) => {
      this.setState({
        name: `remote:${res.data.remote_id} <=> local:${Math.random().toPrecision(6).substring(2)}`
      })
    })
  }

  public render () {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <button onClick={this.click}>Refresh</button>
          <span className="App-link">
            {this.state.name}
          </span>
        </header>
      </div>
    );
  }
}

export default App;
